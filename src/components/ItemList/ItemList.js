import hamburgerImg from '../../assets/images/hamburger.jpg'
import cheeseburgerImg from '../../assets/images/cheeseburger.jpg'
import friesImg from '../../assets/images/fries.jpg'
import coffeeImg from '../../assets/images/coffee.jpg';
import teaImg from '../../assets/images/tea.jpg';
import colaImg from '../../assets/images/cola.jpg';
import JuiceImg from '../../assets/images/juice.jpg';
import HotDogImg from '../../assets/images/hotdog.jpg';
import SandwichImg from '../../assets/images/sandwich.jpg';


const ItemList = () => [
        {name: 'Hamburger', price: 80, image: hamburgerImg},
        {name: 'Cheeseburger', price: 90, image: cheeseburgerImg},
        {name: 'Fries', price: 45, image: friesImg},
        {name: 'Coffee', price: 70, image: coffeeImg},
        {name: 'Tea', price: 50, image: teaImg},
        {name: 'Cola', price: 40, image: colaImg},
        {name: 'Juice', price: 50, image: JuiceImg},
        {name: 'Hot-Dog', price: 60, image: HotDogImg},
        {name: 'Sandwich', price: 70, image: SandwichImg}
    ];

export default ItemList;