import React from 'react';
import './Bill.css';


const Bill = props => {

    return (
        <div className="bill">
            <p className="order-details">Order details</p>
            {props.items.map((item) => {
                let content = null;
                if (item.count > 0) {
                    content = (
                        <div className="total" key={item.name + '-bill'}>
                            <span className="total-name">{item.name}</span>
                            <span className="total-count">{item.count} x {props.itemList.find(x => x.name === item.name).price}</span>
                            <button className="remove-button" onClick={props.removeClick} id={item.name + '-remove'}>x</button>
                        </div>
                    )
                }
                return content;
            })}
            <p className="sum">Sum: {props.sum} KGS</p>
        </div>
    )
};

export default Bill;