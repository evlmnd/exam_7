import Item from '../Item/Item';
import React from 'react';
import './ItemBox.css';

const ItemBox = props => {
    return (
        <div className="item-box">
            {props.items.map((item) => (
                <Item name={item.name}
                      price={item.price}
                      click={(event) => props.addClick(event)}
                      key={item.name}
                      image={item.image}
                      />
            ))}
        </div>
    )
};

export default ItemBox;