import React from 'react';
import './Item.css';

const Item = props => {
    return (
        <div className="item"
             onClick={props.click}
             id={props.name}>
            <img src={props.image} alt=""/>
            <p>{props.name}</p>
            <span>Price: {props.price} KGS</span>
        </div>
    )
};

export default Item;