import React, {Component} from 'react';
import Bill from './components/Bill/Bill';
import ItemBox from './components/ItemBox/ItemBox';
import ItemList from './components/ItemList/ItemList';

import './App.css';

const itemList = new ItemList();


class App extends Component {

    state = {
        items: [
            {name: 'Hamburger', count: 0},
            {name: 'Cheeseburger', count: 0},
            {name: 'Fries', count: 0},
            {name: 'Coffee', count: 0},
            {name: 'Tea', count: 0},
            {name: 'Cola', count: 0},
            {name: 'Juice', count: 0},
            {name: 'Hot-Dog', count: 0},
            {name: 'Sandwich', count: 0}
        ],
        sum: 0
    };


    addItem = event => {
        const items = [...this.state.items];
        let sum = this.state.sum;

        const item = items.find(x => x.name === event.currentTarget.id);
        item.count++;
        const price = itemList.find(x => x.name === event.currentTarget.id).price;
        sum += price;

        this.setState({items, sum});
    };

    removeItemFromBill = event => {
        const items = [...this.state.items];
        let sum = this.state.sum;

        const item = items.find(x => (x.name + '-remove') === event.target.id);
        const price = (itemList.find(x => (x.name + '-remove') === event.target.id)).price;

        if (item.count > 0) {
            item.count -= 1;
            sum-=price;
        }
        this.setState({items, sum});
    };


    render() {
        return (
            <div className="App">
                <Bill items={this.state.items}
                      itemList={itemList}
                      sum={this.state.sum}
                      removeClick={(event) => this.removeItemFromBill(event)}/>
                <ItemBox items={itemList}
                         addClick={this.addItem}/>

            </div>
        );
    }
}

export default App;